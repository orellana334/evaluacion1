
package cl.calcular;


public class Calcular {

        private double valor1;
        private double valor2;
        private double valor3;
        private double calculo;
        
        
    public double getValor1() {
        return valor1;
    }

    /**
     * @param valor1 the valor1 to set
     */
    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }

    /**
     * @return the valor2
     */
    public double getValor2() {
        return valor2;
    }

    /**
     * @param valor2 the valor2 to set
     */
    public void setValor2(double valor2) {
        this.valor2 = valor2;
    }

    /**
     * @return the valor3
     */
    public double getValor3() {
        return valor3;
    }

    /**
     * @param valor3 the valor3 to set
     */
    public void setValor3(double valor3) {
        this.valor3 = valor3;
    }

    /**
     * @return the calculo
     */
    public double getCalculo() {
        double opera = this.valor1 * (this.valor2/100)*this.valor3; 
        return opera;
    }

    /**
     * @param calculo the calculo to set
     */
    public void setCalculo(int calculo) {
        
        
        this.calculo = calculo;
    }


        
        
    
}
