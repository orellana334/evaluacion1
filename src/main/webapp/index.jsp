<%-- 
    Document   : index
    Created on : 21-abr-2021, 23:31:49
    Author     : AOrellana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css"></style>
        <title>Calculo de Intereses</title>
        </head>
    <body>
    <center><div id="centro">
        <table borde="3">
            <table style="width: 50%">
                <tr>
                    <td>
                        <form name="form" action="CalcularControlador" method="GET">
                            <h1>Formulario para Calculo de Interes Simple</h1>
                            <br>
                            <label>Ingrese Capital Simple Producido: </label>
                            <input type="text" id="valor1" name="valor1">
                            <br><br>
                            <label> Ingrese Interes Simple Porducido: </label>   
                            <input type="text" id="valor2" name="valor2">
                            <br><br>
                            <label>Numeros de Años: </label>
                            <input type="text" id="valor3" name="valor3">
                            <br><br>  
                            <button type="submit" class="btn btn-success">Calcular</button>
                            <br>
                        </form>
                    </td>
                </tr>
            </table>
            </div>
            </center>
    </body>
</html>
