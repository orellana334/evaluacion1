<%-- 
    Document   : resultado
    Created on : 22-abr-2021, 1:23:20
    Author     : AOrellana
--%>

<%@page import="cl.calcular.Calcular"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calcular cal = (Calcular)request.getAttribute("calculo");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Capital: <%= cal.getValor1() %>
        <br>
        Porcentaje de Interes: <%= cal.getValor2()%>%
        <br>
        Numero de años: <%= cal.getValor3() %>
        <br>
        Calculo de interes simple es: <%= cal.getCalculo() %>
        <br><br>
    </body>
</html>
